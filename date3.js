window.onload = init;
function init(){
	var datebtn = document.getElementById('submitbtn');
	datebtn.onclick = formatdate;
}
function formatdate(){
	var datestr = document.getElementById('datefield').value;
	var re = /[0-9]{4}.[0-9]{2}.[0-9]{2} [0-9]{1,2}:[0-9]{2}(am|pm)/i;
	if (datestr.match(re)){
		var parts = datestr.split(' ');
		var dt = document.getElementById('date');
		console.log(parts);
		console.log('Date: '+parts[0]+', Time: '+parts[1]);
		dt.innerHTML = 'Date: '+parts[0]+', Time: '+parts[1];		
		
	}
	else{
		alert('Input does not match date format.')
	}
}
